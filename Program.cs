﻿using System;

class Program
{
    static int[] numbers = new int[6];
    static int count = 0;

    static void Main()
    {
        bool exit = false;

        while (!exit)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("1. Xem danh sách");
            Console.WriteLine("2. Thêm số mới");
            Console.WriteLine("3. Xóa 1 số");
            Console.WriteLine("4. Thoát");

            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    DisplayNumbers();
                    break;

                case 2:
                    AddNewNumber();
                    break;

                case 3:
                    RemoveNumber();
                    break;

                case 4:
                    exit = true;
                    Console.WriteLine("Thoát chương trình.");
                    break;

                default:
                    Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng thử lại.");
                    break;
            }
        }
    }

    static void DisplayNumbers()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        Console.WriteLine("Danh sách các số: ");
        for (int i = 0; i < count; i++)
        {
            Console.Write(numbers[i] + " ");
        }
        Console.WriteLine();
    }

    static void AddNewNumber()
    {
        int newNumber;
        do
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Write("Nhập số cần thêm (6 chữ số): ");
            newNumber = Convert.ToInt32(Console.ReadLine());

        } while (newNumber.ToString().Length != 6);

        if (count == numbers.Length)
            Array.Resize(ref numbers, numbers.Length * 2);

        numbers[count++] = newNumber;
        Console.WriteLine("Số đã được thêm vào danh sách.");
    }

    static void RemoveNumber()
    {
        if (count == 0)
        {
            Console.WriteLine("Danh sách rỗng. Không có số để xóa.");
            return;
        }

        Console.OutputEncoding = System.Text.Encoding.UTF8;
        Console.Write("Nhập số cần xóa: ");
        int numberToRemove = Convert.ToInt32(Console.ReadLine());

        int index = Array.IndexOf(numbers, numberToRemove);

        if (index != -1)
        {
            for (int i = index; i < count - 1; i++)
            {
                numbers[i] = numbers[i + 1];
            }
            count--;
            Console.WriteLine($"Số {numberToRemove} đã được xóa khỏi danh sách.");
        }
        else
        {
            Console.WriteLine("Không tìm thấy số trong danh sách.");
        }
    }
}
